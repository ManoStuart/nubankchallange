(ns nubank.graph-test
  (:require [clojure.test :refer :all]
            [nubank.graph.graph :refer :all]
            [nubank.graphExamples :refer :all])
  (:import nubank.graph.graph.Graph))

(defn- equal-map? [collection values]
  (if (= (count collection) (count values))
    (reduce (fn [_ value]
      (if (contains? collection value)
        true
        (reduced false)))
      true values)
    false))

(defn- equal-seq? [collection values]
  (if (= (count collection) (count values))
    (reduce (fn [_ value]
      (if (= (.indexOf collection value) -1)
        (reduced false)
        true))
      true values)
    false))

(deftest test-graph
  (testing "simple graph"
    (let [graph (new-graph (simpleGraph))]
      (is (equal-map? (vertices graph) [1 2 3 4 5]))
      (is (equal-seq? (edges graph) [[1 4] [1 2] [4 1] [4 5] [3 2] [2 1] [2 3] [5 4]]))

      (is (has-vertex? graph 1))
      (is (has-vertex? graph 2))
      (is (has-vertex? graph 3))
      (is (has-vertex? graph 4))
      (is (has-vertex? graph 5))
      (is (not (has-vertex? graph 6)))

      (is (has-edge? graph 1 2))
      (is (has-edge? graph 1 4))
      (is (has-edge? graph 2 3))
      (is (has-edge? graph 4 5))
      (is (not (has-edge? graph 1 5)))

      (is (equal-map? (successors graph 1) [2 4]))
      (is (equal-seq? (vertex-edges graph 1) [[1 2] [1 4]]))))

  (testing "graph operations"
    (let [graph (new-graph (simpleGraph))]
      ; Simple edge adition
      (is (equal-map? (vertices (add-edges graph [[2 4] [3 6]])) [1 2 3 4 5 6]))

      ; Existing edge adition
      (is (= (count (vertex-edges (add-edges graph [[1 4]]) 1)) 2))

      ; Simple edge removal.
      (is (= (count (vertex-edges (remove-edges graph [[1 4]]) 1)) 1))

      ; Non-existing edge removal.
      (is (equal-map? (vertices (remove-edges graph [[2 4] [3 6]])) [1 2 3 4 5]))
      (is (= (count (edges (remove-edges graph [[2 4] [3 6]]))) 8))

      ; edge removal until vertex has no connections.
      (is (equal-map? (vertices (remove-edges graph [[1 2] [1 4]])) [1 2 3 4 5]))
      (is (= (count (vertex-edges (remove-edges graph [[1 2] [1 4]]) 1)) 0))

      ; simple node removal
      (is (equal-map? (vertices (remove-vertices graph [1])) [2 3 4 5]))
      (is (= (count (vertex-edges (remove-vertices graph [1]) 1)) 0))
      (is (= (count (vertex-edges (remove-vertices graph [1]) 2)) 1))

      ; Graph purge
      (is (= (remove-vertices graph (vertices graph)) (Graph. #{} {})))

      (is (= (remove-all graph) (Graph. #{} {})))))

  (testing "strange vertex names"
    (let [graph (new-graph [[:a "b"] [1 "jonny be gud"] [:a 1] ["jonny be gud" nil]])]
      (is (equal-map? (vertices graph) [:a "b" 1 "jonny be gud" nil]))

      (is (has-vertex? graph :a))
      (is (has-vertex? graph "b"))
      (is (has-vertex? graph 1))
      (is (has-vertex? graph "jonny be gud"))
      (is (has-vertex? graph nil))

      ; Simple edge adition
      (is (equal-map? (vertices (add-edges graph [[nil 0.3]])) [:a "b" 1 "jonny be gud" nil 0.3]))))

  (testing "disconnected graph"
    (let [graph (new-graph (disconnectedGraph))]
      (is (= (count (vertices graph)) 5))
      (is (= (count (edges graph)) 6))

      (is (has-vertex? graph 1))
      (is (not (has-vertex? graph 0)))
      (is (has-vertex? graph 5))
      (is (not (has-vertex? graph 6)))))

  (testing "default graph"
    (let [graph (new-graph (defaultGraph))]
      (is (= (count (vertices graph)) 100))

      ; Input has 945 edges, but 47 of them are duplicated.
      (is (= (count (edges graph)) 1796))

      (is (has-vertex? graph 0))
      (is (not (has-vertex? graph -1)))
      (is (has-vertex? graph 99))
      (is (not (has-vertex? graph 100))))))