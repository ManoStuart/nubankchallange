(ns nubank.performance-test
  (:require [clojure.test :refer :all]
            [nubank.graph.socialGraph :refer :all]
            [nubank.graphExamples :refer :all]))

(defmacro timed [expr & label]
  (let [sym (= (type expr) clojure.lang.Symbol)]
    `(let [start# (. System (nanoTime))
           return# ~expr
           res# (if ~sym  
                  (resolve '~expr)  
                  (resolve (first '~expr)))
           time# (/ (double (- (. System (nanoTime)) start#)) 1000000.0)]
       (prn (str "Timed " (or ~@label (:name (meta res#))) ": " time# " msecs"))
       [return# time#])))

(deftest test-performance
  (testing "default graph performance"
    (prn "Started default graph performance test:")
    (let [defaultTime (timed (new-social-graph (defaultGraph)) "Default graph creation")
          addEdgeTime (timed (social-add-edges (first defaultTime) [[1 5]]) "Add edge")
          recalculationTime (timed (new-social-graph (conj (defaultGraph) [1 5])) "Recalculation time")]

      ; performance comparison
      (is (< (last addEdgeTime) (last recalculationTime)))))

  (testing "big graph performance"
    (prn "Started big graph performance test:")
    (let [defaultTime (timed (new-social-graph (bigGraph)) "Big graph creation")
          addEdgeTime (timed (social-add-edges (first defaultTime) [[1 5]]) "Add edge")
          recalculationTime (timed (new-social-graph (conj (bigGraph) [1 5])) "Recalculation time")]

      ; performance comparison
      (is (< (last addEdgeTime) (last recalculationTime))))))