(ns nubank.socialGraph-test
  (:require [clojure.test :refer :all]
            [nubank.graph.socialGraph :refer :all]
            [nubank.graphExamples :refer :all]))

(deftest test-social
  (testing "simple graph"
    (let [graph (new-social-graph (simpleGraph))
          disconnected (new-social-graph (disconnectedGraph))]

      (is (=
        (:scores graph)
        {1 0.16666666666666666, 4 0.14285714285714285, 3 0.1, 2 0.14285714285714285, 5 0.1}))

      ; simple edge adition
      (is (= (social-add-edges graph [[1 5]]) (new-social-graph (conj (simpleGraph) [1 5]))))

      ; multiple edge adition
      (is (= (social-add-edges graph [[1 5] [2 5]]) (new-social-graph (conj (simpleGraph) [1 5] [2 5]))))

      ; existing edge adition
      (is (= (social-add-edges graph [[1 4]]) graph))

      ; edge to new vertex adition
      (is (= (social-add-edges graph [[6 3]]) (new-social-graph (conj (simpleGraph) [3 6]))))

      ; simple edge removal - makes 2 disconnected graph
      (is (= (social-remove-edges graph [[1 4]]) disconnected))

      ; multiple edge removal - leaves 2 vertices without edges
      (is (=
        (:scores (social-remove-edges graph [[1 4] [4 5]]))
        {1 0.3333333333333333, 4 Double/POSITIVE_INFINITY, 3 0.3333333333333333, 2 0.5, 5 Double/POSITIVE_INFINITY}))

      ; nonexistent edge removal
      (is (= (social-remove-edges graph [[3 6]]) graph))

      ; simple fraud
      (is (=
        (:scores (make-fraudulent graph 1))
        {1 0.0, 4 0.07142857142857142, 3 0.07500000000000001, 2 0.07142857142857142, 5 0.07500000000000001}))

      ; nonexisting vertex fraud
      (is (= (make-fraudulent graph 6) graph))

      ; disconnected graphs fraud
      (is (=
        (:scores (make-fraudulent disconnected 5))
        {1 0.3333333333333333, 4 0.5, 3 0.3333333333333333, 2 0.5, 5 0.0}))))

  (testing "default graph"
    (let [graph (new-social-graph (defaultGraph))]
      ; simple edge adition
      (is (= (social-add-edges graph [[1 5]]) (new-social-graph (conj (defaultGraph) [1 5]))))

      ; simple edge removal
      (is (= (social-remove-edges graph [[64 48]]) (new-social-graph (pop (defaultGraph)))))))

  (testing "big graph"
    (let [graph (new-social-graph (bigGraph))]
      ; simple edge adition
      (is (= (social-add-edges graph [[1 5]]) (new-social-graph (conj (bigGraph) [1 5]))))

      ; simple edge removal
      (is (= (social-remove-edges graph [[0 12]]) (new-social-graph (pop (bigGraph))))))))