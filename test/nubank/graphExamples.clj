(ns ^{:doc "Define default inputs to be used in the unit tests."
      :author "Arthur Espindola Ribeiro"}
  nubank.graphExamples)

(defn simpleGraph []
  [[1 2] [2 3] [4 5] [1 4]])

(defn disconnectedGraph []
  [[1 2] [2 3] [4 5]])

(defn defaultGraph []
  (read-string (slurp "test/nubank/graphs/defaultGraph.edn")))

(defn bigGraph []
  (read-string (slurp "test/nubank/graphs/bigGraph.edn")))