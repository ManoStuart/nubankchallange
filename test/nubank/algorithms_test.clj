(ns nubank.algorithms-test
  (:require [clojure.test :refer :all]
            [nubank.graph.graph :refer :all]
            [nubank.graph.algorithms :refer :all]
            [nubank.graphExamples :refer :all]))

(deftest test-algorithms
  (testing "simple graph"
    (let [graph (new-graph (simpleGraph))
          distanceMatrix (floyd-warshall graph)]
      (is (= distanceMatrix (get-all-distances graph)))
      (is (= (get distanceMatrix 1) {1 0, 4 1, 3 2, 2 1, 5 2}))))

  (testing "disconnected graph"
    (let [graph (new-graph (disconnectedGraph))
          distanceMatrix (floyd-warshall graph)]
      (is (= distanceMatrix (get-all-distances graph)))
      (is (= (get distanceMatrix 1) {1 0, 4 Integer/MAX_VALUE, 3 2, 2 1, 5 Integer/MAX_VALUE}))))

  (testing "default graph"
    (let [graph (new-graph (defaultGraph))]
      (is (= (floyd-warshall graph) (get-all-distances graph))))))