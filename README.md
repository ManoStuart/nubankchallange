# nubank - graph ranking challange

Implementation in Clojure of a RESTful api, to rank vertices of a graph, based on their centrality metrics and fraudulent statuses.

## Prerequisites

You will need [Leiningen][] 2.0.0 or above installed.
You will need [MongoDB][] 3.0.11 or above installed.

[leiningen]: https://github.com/technomancy/leiningen
[mongodb]: https://docs.mongodb.org/manual/installation

## Running

To start a web server for the application, run:

    lein ring server

The application should now be ready at port 3000.


## API

We used the library compojure to implement the RESTful API, which provides a clean sintax to declare our endpoints, and middleware to handle json requests.

**POST** arguments should be supplied as a json in the body.
**GET** arguments should be supplied as url queries.

every call will yield a json as response.

An edge is a vector of exactly 2 positions [u v] / u and v are vertex ids.
A vertex id can be anything.


#### Authentication

  TODO


#### Endpoints

* **POST /create-graph**

    Creates a graph from scratch with the supplied edges.

    ###### Request

    |   Property    |       Type     |      Description       |
    | ------------- |----------------| -----------------------|
    | edges         | array of edges | Initial edges of graph |

    *sample*
    ```
    curl -X POST localhost:3000/create-graph \
    -H "Content-Type:application/json" \
    -d '{"edges": [[1, 2],[2, 3],[1, 4],[4, 5]]}'
    ```

* **POST /add-edges**

    Adds supplied edges to current stored graph.

    ###### Request

    |   Property    |       Type     |         Description         |
    | ------------- |----------------| ----------------------------|
    | edges         | array of edges | Edges of graph for be added |

    *sample*
    ```
    curl -X POST localhost:3000/add-edges \
    -H "Content-Type:application/json" \
    -d '{"edges": [[1, 2],[2, 3],[1, 4],[4, 5]]}'
    ```

* **POST /delete-graph**

    Purges stored graph.

    ###### Request

    *sample*
    ```
    curl -X POST localhost:3000/delete-graph \
    -H "Content-Type:application/json"
    ```

* **POST /fraudulent**

    Flags a vertex as fraudulent.

    |   Property    |       Type     |         Description         |
    | ------------- |----------------| ----------------------------|
    | vertex        | vertex id      | Id of the fraudulent vertex |

    ###### Request

    *sample*
    ```
    curl -X POST localhost:3000/fraudulent \
    -H "Content-Type:application/json"
    -d '{"vertex": 1}'
    ```

* **GET /ranking**

    Returns the vertices of the current graph, ordered by their scores, using their id's as tiebreaker.


    ###### Request

    |   Property    |       Type       |         Description              |
    | ------------- |------------------| ---------------------------------|
    | limit         | integer          | Max number of vertices to return |
    | skip          | integer          | Number of vertices to skip       |
    | fields        | array of strings | vertex info to return            |

    Parameters defaults to: limit = 10, skip = 0, fields = ["id"]

    *sample*
    ```
    curl -X GET "localhost:3000/ranking?limit=3&skip=2&fields=id,score" \
    -H "Content-Type:application/json" \
    ```

    ###### Response
    
    Returns an array of vertices with the proprieties(fields) especified in the request.

    |   Property    |      Type      |             Description               |
    | ------------- |----------------| --------------------------------------|
    | id            | string         | The id assigned to the vertex         |
    | score         | double         | The current score of the vertex       |
    | fraud         | bool           | If the vertex is flaged as fraudulent |

    *sample*
    ```json
    [
        {
            "score": 0.14285714285714285
            "id": 4
        },
        {
            "score": 0.1
            "id": 3
        },
        {
            "score": 0.1
            "id": 5
        }
    ]
    ```



## Solution Outline

#### The Graph

The Graphs in our problem are undirected and unweighted. The vertex id's can take any form, and duplicated edges are meaningless.

We represent the graph in memory as a map from the vertex's id to it's adjacency map.

Since the goal is to rank the graph by the vertices scores, we also keep this data, plus any vertex flaged as fraudulent, thus avoiding constant recalculations.

#### Single Source Spanning Tree (sssp)

From the computation of a single source spanning tree, produce a list with the distances from all vertices to the tree's source, and so calculate the source's centrality, by adding all distances and inversing the result.

Then, for each fraudulent vertex, calculate it's influence according to the formula:
    F(k) = (1 - (1/2)^k) , where k is the distance to the source

and apply it to the centrality, generating the source's score.

#### Floyd-Warshall

The algorithm of floyd-warshall was also implemented, but it proved to be slower than the previous one, and so it is only being used now to compare the results.

#### Closeness and Farness

From the [reference](https://en.wikipedia.org/wiki/Centrality#Closeness_centrality) provided, I gathered that closeness is only relevant in a connected graph, so when calculating the farness, we disregard disconnected vertices.

We also define that the closeness of a isolated vertex is Infinity.

#### Work Filter

During the creation of the graph we are required to run the sssp for every vertex. But on subsequent edge modifications not every vertex has it's score altered.
This [paper](http://bmi.osu.edu/hpc/papers/Sariyuce13-BigData.pdf) shows a way to filter out said vertices, and greatly reduce the recomputation time. The algorithm 2 was implemented and the nubank.performance-test clearly shows it's impact.
 

#### Database

My first ideia was to use a graph oriented database, like neo4j, but due to the restriction of using graph libraries, I decided not to.

I decided then to go with mongo, due to it's no schema implementation. Since we are supposedly doing a "social network analysis", It's fair to assume that in a production implementation we would also be storing people's info, and these can vary a lot due to the quantity of available sources, thus making a no-schema approach advisable.

The "monger" wrapper proved easy to use, providing a simple conversion between clojure and mongo objects. The wrapper did not encompass the whole MongoDB implementation though.

Each document stored in the database represents a vertex, storing it's id, adjacency, score and if it is a fraud. This data model can easely extend to fit other requirements the system might come to need.

That being said, the current integration covers only the essential required to run the API endpoints requested.

## Tests

To run all tests, do:

    lein test

The tests are located at test/nubank/* , and can run separately:

    lein test :only test.namespace

for instance:

    lein test :only nubank.graph-test
