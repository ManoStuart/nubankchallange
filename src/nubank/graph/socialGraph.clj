(ns ^{:doc "Creates a 'Social Graph' atop the default IGraph protocol.
  Adding the concept of closeness and fraudulent vertices."
      :author "Arthur Espindola Ribeiro"}
  nubank.graph.socialGraph
  (:require [nubank.graph.algorithms :refer [get-all-distances sssp]]
            [clojure.math.numeric-tower :refer [expt abs]]
            [nubank.graph.graph :refer :all])
  (:import nubank.graph.graph.Graph))

;;;
;;; Score Calculation
;;;

(defn vertex-farness
  "Returns the farness of a vertex.
   Farness = sum of all distances from each vertex to supplied vertex."
  ([distances]
    (reduce (fn [count val]
      (if (= val Integer/MAX_VALUE)
        count
        (+ count val)))
      0 (vals distances)))
  ([graph vertex] (vertex-farness (sssp graph vertex))))

(defn vertex-closeness
  "Returns the closeness of a vertex.
   Closeness = 1/Farness,
   Farness = sum of all distances from each vertex to supplied vertex."
  ([distances] (/ 1.0 (vertex-farness distances)))
  ([graph vertex] (vertex-closeness (sssp graph vertex))))

(defn- vertex-fraud-coeficient
  "Get the resulting multiplier coeficient that all fraudulent vertices exerts upon the supplied vertex.
   The distances collection must be referent that vertex."
  ([graph distances vertex]
    (reduce
      (fn [multi distance]
        (if (get (:frauds graph) vertex)
          (* multi (- 1 (expt 0.5 (get distances vertex))))
          multi))
      1 distances))
  ([graph vertex] (vertex-fraud-coeficient graph (sssp graph vertex) vertex)))

(defn- single-fraud-coeficient
  "Returns the coefficient that a fraudulent vertex exerts upon the supplied vertex.
   The distances collection must be referent to the fraudulent vertex."
  [distances vertex]
  (- 1 (expt 0.5 (get distances vertex))))

(defn- vertex-score
  "Returns the score of a vertex.
   = closeness * (fraud-coeficient)"
  ([graph vertex]
    (let [distances (sssp graph vertex)]
      (* (vertex-closeness distances) (vertex-fraud-coeficient graph distances vertex))))
  ([graph distances vertex] (* (vertex-closeness distances) (vertex-fraud-coeficient graph distances vertex))))

;;;
;;; Graph Creation
;;;

(defn- generate-scores
  "Populates the score of every vertex in the graph."
  [graph]
  (let [distanceMap (get-all-distances graph)]
    (reduce
      (fn [g vertex]
        (assoc-in g [:scores vertex] (vertex-score g (get distanceMap vertex) vertex)))
      graph (vertices graph))))

(defn new-social-graph
  "Creates a graph, then calculates the scores of each vertex."
  ([] (assoc (Graph. #{} {}) :scores {} :frauds {}))
  ([edges] (generate-scores (add-edges (new-social-graph) edges))))

;;;
;;; Work Filter
;;;

(defn- simple-work-filter
  "Filters which vertices should have their scores recalculated when edge [u v] is modified.
   The call when the edge is added/removed differs,
   as specified in the paper 'Incremental Algorithms for Closeness Centrality' by Ahmet E. Sarıyuce"
  [graph [u v]]
  (let [du (sssp graph u)
        dv (sssp graph v)]
    (reduce
      (fn [modified vertex]
        (if (> (abs (- (get du vertex) (get dv vertex))) 1)
          (conj modified vertex)
          modified))
      (if (has-vertex? graph u) (if (has-vertex? graph v) [] [v]) [u])
      (vertices graph))))

;;;
;;; Add Edges
;;;

(defn- recalculate-scores-add
  "Wrapper for the work-filter on edge adition."
  [graph edge]
  (reduce
    (fn [g vertex]
      (assoc-in g [:scores vertex] (vertex-score g vertex)))
    (add-edge graph edge) (simple-work-filter graph edge)))

(defn social-add-edges
  "Adds edges to the graph, and recalculates the vertices scores."
  [graph edges]
  (reduce
    (fn [g e]
      (recalculate-scores-add g e))
    graph edges))

;;;
;;; Remove Edges
;;;

(defn- recalculate-scores-remove
  "Wrapper for the work-filter on edge removal."
  [graph edge]
  (let [newGraph (remove-edge graph edge)]
    (reduce
      (fn [g vertex]
        (assoc-in g [:scores vertex] (vertex-score g vertex)))
      newGraph (simple-work-filter newGraph edge))))

(defn social-remove-edges
  "Remove edges from the graph, and recalculates the vertices scores."
  [graph edges]
  (reduce
    (fn [g [u v]]
      (if (has-edge? g u v)
        (recalculate-scores-remove g [u v])
        g))
    graph edges))

;;;
;;; Flag Frauds
;;;

(defn make-fraudulent
  "Flags a vertex as fraudulent, and recalculates the vertices scores."
  [graph vertex]
  (if (has-vertex? graph vertex)
    (let [distances (sssp graph vertex)]
      (reduce
        (fn [g v]
          (assoc-in g [:scores v] (* (get (:scores g) v) (single-fraud-coeficient distances v))))
        (assoc-in graph [:frauds vertex] true) (vertices graph)))
    graph))