(ns ^{:doc "Graph algorithms (Single source spanning tree, floyd-warshall) implementations"
      :author "Arthur Espindola Ribeiro"}
  nubank.graph.algorithms
  (:require [nubank.graph.graph :refer :all])
  (:import nubank.graph.graph.Graph))

;;;
;;; Spanning Trees
;;;

(defn- init-infinite
  "Initiates a distance map with MAX_VALUE for every vertex on the graph, but the supplied vertex"
  [graph vertex]
  (reduce
    (fn [distances v]
      (if (= vertex v)
        distances
        (conj distances {v Integer/MAX_VALUE})))
    {vertex 0} (vertices graph)))

(defn- enqueue-bfs
  "Enqueues any successors from vertex that hasn't been evaluated yet."
  [graph vertex queue distances]
  (let [newDist (inc (get distances vertex))]
    (reduce
      (fn [[q d] v]
        (if (= (get distances v) Integer/MAX_VALUE)
          [(conj q v) (assoc d v newDist)]
          [q d]))
      [queue distances] (successors graph vertex))))

; Single source spanning tree
(defn sssp
  "Returns a map with the minimum distance from the supplied vertex to every vertex in the graph."
  [graph vertex]
  (loop [queue (conj clojure.lang.PersistentQueue/EMPTY vertex)
         distances (init-infinite graph vertex)]
    (if (empty? queue)
      distances
      (let [v (peek queue)
            [q d] (enqueue-bfs graph v queue distances)]
        (recur (pop q) d)))))

(defn get-all-distances
  "Returns a map with the minimum distance from each vertex to every vertex in the graph.
   Calculates multiple spanning trees internally."
  [graph]
  (reduce
    (fn [distanceMap vertex]
      (conj distanceMap {vertex (sssp graph vertex)}))
    {} (vertices graph)))


;;;
;;; Floyd Warshall
;;;

(defn- init-immediate-distance
  "Returns a collection of the immediate distances of a vertex.
   Vertices with a direct edge to the main vertex will have distance 1, otherwise distance = Infinity"
  [graph vertex]
  (let [adj (successors graph vertex)]
    (reduce
      (fn [distances v]
        (if (= vertex v)
          (assoc distances v 0)
          (if (get adj v)
            (assoc distances v 1)
            (assoc distances v Integer/MAX_VALUE))))
      {} (vertices graph))))

(defn- floyd-warshall-init
  "Initiates the base matrix to be used by the floyd-warshall function."
  [graph]
  (reduce
    (fn [distanceMap vertex]
      (assoc distanceMap vertex (init-immediate-distance graph vertex)))
    {} (vertices graph)))

(defn floyd-warshall
  "Returns a map with the minimum distance from each vertex to every vertex in the graph,
   using the floyd-wharshall algorithm."
  [graph]
  (reduce (fn [distanceMap vk]
    (reduce (fn [distanceMap vi]
      (reduce (fn [distanceMap vj]
        (let [newDist (+ (get-in distanceMap [vi vk]) (get-in distanceMap [vk vj]))]
          (if (> (get-in distanceMap [vi vj]) newDist)
            (assoc-in (assoc-in distanceMap [vi vj] newDist) [vj vi] newDist)
            distanceMap)))
        distanceMap (vertices graph)))
      distanceMap (vertices graph)))
    (floyd-warshall-init graph) (vertices graph)))