(ns ^{:doc "Simple storage of social graphs using Monger, a wrapper for MongoDB."
      :author "Arthur Espindola Ribeiro"}
  nubank.graph.mongo
  (:refer-clojure :exclude [sort find])
  (:require [monger.core :as mg]
            [monger.collection :as mc]
            [monger.query :as query]
            [nubank.graph.graph :refer :all]
            [nubank.graph.socialGraph :refer [new-social-graph]])
  (:import nubank.graph.graph.Graph))

;;;
;;; Default connections
;;;

(def conn (mg/connect))
(def db (mg/get-db conn "nubank"))

;;;
;;; Store Graph
;;;

(defn- get-vertex-document
  "Parses a clojure graph object to a mongo vertex document."
  [graph vertex]
  { :id vertex
    :adj (get-in graph [:adj vertex])
    :score (get-in graph [:scores vertex])
    :fraud (get-in graph [:frauds vertex])})

(defn store-graph
  "Stores the graph in mongo.
   Does not check if there is an existing graph already stored, and will only override the vertices found in the supplied graph."
  [graph]
  (doseq [vertex (vertices graph)]
    (mc/update db "vertices" {:id vertex} (get-vertex-document graph vertex) {:upsert true})))

;;;
;;; Load Graph
;;;

(defn load-graph
  "Loads the stored graph. Always returns a Graph. obj."
  []
  (let [docs  (mc/find-maps db "vertices")
        graph (new-social-graph)]

    (reduce
      (fn [g n]
        (let [g1 (-> g
                (update-in [:vertexMap] conj (:id n))
                (assoc-in [:adj (:id n)] (set (:adj n)))
                (assoc-in [:scores (:id n)] (:score n)))]
          (if (:fraud n)
            (assoc-in g1 [:frauds (:id n)] (:fraud n))
            g1)))
      graph docs)))

;;;
;;; Purge Graph
;;;

(defn drop-graph
  "Purges the current graph from the database."
  []
  (mc/remove db "vertices"))

;;;
;;; Graph Ranking
;;;

(defn- remove-mongo-id
  "Removes the ObjectId that mongo generates by default.
   Currently there is no option in the monger api to remove it, as there is in the pure mongo implementation"
  [docs]
  (for [doc docs]
    (dissoc doc :_id)))

(defn get-ranking
  "Returns the vertices of the current graph, ordered by their scores, using their id's as tiebreaker.
   You may supply a limit for the returned vertices, an ammount to skip and which fields to include in the response.
   defaults: limit = 10, skip = 0, fields = 'id'."
  [& {:keys [limit fields skip] :or {limit 10 fields [:id] skip 0}}]
  (remove-mongo-id
    (query/with-collection db "vertices"
      (query/find {})
      (query/sort (array-map :score -1 :id 1))
      (query/fields fields)
      (query/limit limit)
      (query/skip skip))))