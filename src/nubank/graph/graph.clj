(ns ^{:doc "Defines a protocol for a undirected unweighted graph"
      :author "Arthur Espindola Ribeiro"}
  nubank.graph.graph)

(defprotocol IGraph
  (vertices [graph] "Returns a collection of the vertices in the graph.")
  (edges [graph] "Returns a collection of the edges in the graph. Returns both [u v] and [v u].")
  (has-vertex? [graph vertex] "Returns true when vertex is in graph.")
  (has-vertices? [graph vertex] "Returns true when all vertices are in graph.")
  (has-edge? [graph u v] "Returns true when edge [u v] is in grap.")
  (has-edges? [graph edges] "Returns true when all edges are in grap.")
  (successors [graph vertex] "Returns direct successors of vertex.")
  (vertex-edges [graph vertex] "Returns all the edges of vertex.")
  (add-vertices [graph vertices] "Add vertices to graph.")
  (add-edge [graph [u v]] "Add single edge to graph.")
  (add-edges [graph edges] "Add edges to graph.")
  (remove-vertices [graph vertices] "Remove vertices from graph.")
  (remove-edge [graph [u v]] "Removes a single edge from graph.")
  (remove-edges [graph edges] "Removes edges from graph.")
  (remove-all [graph] "Removes all vertices and edges from graph."))


(defn- remove-adj-vertices [adj vertices adjacents]
  (reduce
   (fn [adj n]
     (if (adj n)
       (update-in adj [n] #(apply disj % vertices))
       adj))
   (apply dissoc adj vertices) adjacents))

(defrecord Graph [vertexMap adj]
  IGraph
  (vertices [graph] (:vertexMap graph))

  (edges [graph] (for [v (vertices graph) e (vertex-edges graph v)] e))

  (has-vertex? [graph vertex] (contains? (:vertexMap graph) vertex))

  (has-vertices? [graph vertices] (reduce (fn [_ vertex] (if (has-vertex? graph vertex) true (reduced false))) true vertices))

  (has-edge? [graph u v] (contains? (get-in graph [:adj u]) v))

  (has-edges? [graph edges] (reduce (fn [_ [u v]] (if (has-edge? graph u v) true (reduced false))) true edges))

  (successors [graph vertex] (get-in graph [:adj vertex]))

  (vertex-edges [graph vertex] (for [v (successors graph vertex)] [vertex v]))

  (add-vertices [graph vertices]
    (reduce
      (fn [graph v]
        (-> graph
          (update-in [:vertexMap] conj v)
          (assoc-in [:adj v] (or ((:adj graph) v) #{}))))
      graph vertices))

  (add-edge [graph [u v]]
    (if (has-edge? graph u v)
      ; (do (println u v) graph)
      graph
      (-> graph
        (update-in [:vertexMap] conj u v)
        (update-in [:adj u] (fnil conj #{}) v)
        (update-in [:adj v] (fnil conj #{}) u))))

  (add-edges [graph edges]
    (reduce add-edge graph edges))

  (remove-vertices [graph vertices]
    (let [adjacents (mapcat #(successors graph %) vertices)]
      (-> graph
        (update-in [:vertexMap] #(apply disj % vertices))
        (assoc :adj (remove-adj-vertices (:adj graph) vertices adjacents)))))

  (remove-edge [graph [u v]]
    (if (has-edge? graph u v)
      (-> graph
        (update-in [:adj u] disj v)
        (update-in [:adj v] disj u))
      graph))

  (remove-edges [graph edges]
    (reduce remove-edge graph edges))

  (remove-all [graph] (assoc graph :vertexMap #{} :adj {})))

(defn graph?
  "Returns true if graph satisfies the IGraph protocol"
  [graph]
  (satisfies? IGraph graph))

(defn new-graph
  "Creates an unweighted, undirected graph. inits must be edges"
  ([] (Graph. #{} {}))
  ([edges] (add-edges (Graph. #{} {}) edges)))
