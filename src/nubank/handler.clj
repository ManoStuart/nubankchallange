(ns nubank.handler
  (:require [compojure.core :refer :all]
            [compojure.route :as route]
            [compojure.handler :as handler]
            [ring.middleware.json :refer [wrap-json-response wrap-json-body]]
            [nubank.routes.ranking :as ranking]
            [nubank.routes.apiGraph :as apiGraph]))

(defroutes app-routes
  ranking/routes
  apiGraph/routes
  (route/not-found "Not Found"))

(def app
  (-> app-routes
    handler/api
    wrap-json-response
    wrap-json-body))