(ns nubank.routes.apiGraph
  (:require [compojure.core :refer [GET POST defroutes]]
            [ring.util.response :refer [response]]
            [nubank.graph.graph :refer [add-edges]]
            [nubank.graph.algorithms :refer [floyd-warshall]]
            [nubank.graph.socialGraph :refer :all]
            [nubank.graph.mongo :as mongo]))

(defn- parse [request field]
  (get (:body request) field))

(defn- response-wrapper [newGraph]
  (mongo/store-graph newGraph)
  (response (:scores newGraph)))

(defn- create-graph [request]
  (response-wrapper (new-social-graph (parse request "edges"))))

(defn- delete-graph [request]
  (response (mongo/drop-graph)))

(defn- api-add-edges [request]
  (response-wrapper (social-add-edges (mongo/load-graph) (parse request "edges"))))

(defn- fraudulent [request]
  (response-wrapper (make-fraudulent (mongo/load-graph) (parse request "vertex"))))

(defn- get-graph [_]
  (response (mongo/load-graph)))

(defroutes routes
  (POST "/create-graph" [] create-graph)
  (POST "/delete-graph" [] delete-graph)
  (POST "/add-edges" [] api-add-edges)
  (POST "/fraudulent" [] fraudulent)

  (GET "/graph" [] get-graph))