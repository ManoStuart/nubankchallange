(ns nubank.routes.ranking
  (:require [compojure.core :refer [GET POST defroutes]]
            [ring.util.response :refer [response]]
            [nubank.graph.mongo :refer [get-ranking]]))

(defn- parse-params [params]
  (reduce-kv (fn [vec k v]
    (if (= k :fields)
      (conj vec k (for [key v] (keyword key)))
      (conj vec k (read-string v))))
    [] params))
  
(defn- ranking [params]
  (response (apply get-ranking (parse-params params))))

(defn- single-ranking [request]
  (response (apply get-ranking request)))

(defroutes routes
  (GET "/ranking" {params :params} (ranking params))
  (GET "/ranking/:id" [] single-ranking))